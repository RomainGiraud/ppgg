#!/usr/bin/env ruby

class String
  def purify
    self.gsub(/[^a-z0-9_]+/, '_')
        .gsub(/^([0-9]+)/, '_\1')
  end
end

pkg_input = ""
if ARGV.length > 0
  pkg_input = ARGV[0]
  puts "Generate graph for #{pkg_input}"
else
  puts "Generate graph for installed packages"
end

puts "Extract information..."
pkg = []
%x{pacman -Q #{pkg_input}}.split("\n").each do |line|
  array = line.split(/[\/ ]/)

  req = %x{pacman -Qi #{array[0]}}.scan(/Required By *: (.*)$/).first.first.split(' ')
  req = [] if req[0] == "None"

  pkg << {
    name: array[0].to_s,
    required: req
  }
end

puts "Generate dot file..."
File.open('packages.dot', 'w') do |file|
  file.write "digraph G {\n"
  file.write "  rankdir=LR;\n"
  pkg.each do |p|
    if p[:required].empty?
      file.write "  " + p[:name].purify + ";\n"
    else
      file.write "  " + p[:name].purify + " -> {" + p[:required].map{|s| s.purify}.join('; ') + ";};\n"
    end
  end
  file.write "}\n"
end

puts 'You must generate graph with graphviz:'
puts '  dot -Tpng packages.dot > packages.png'
