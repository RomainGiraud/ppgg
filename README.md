<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Pacman package graph generator</h3>

  <p align="center">
    Generate a dependency graph of a package
    <br />
    <a href="https://gitlab.com/RomainGiraud/ppgg"><strong>Explore the docs »</strong></a>
    <br />
    <a href="https://gitlab.com/RomainGiraud/ppgg/-/issues">Report Bug or Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project

I made this tool to help me visualize package dependencies. I hope it cans help you too!


### Built With

* [Ruby](https://www.ruby-lang.org)
* [Graphviz](https://graphviz.org/)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* Ruby environment
* Graphviz package

### Installation

Clone the repo
```sh
git clone https://gitlab.com/RomainGiraud/ppgg.git
```



<!-- USAGE EXAMPLES -->
## Usage

You can pass a package name as an argument to the script.
```sh
./main.rb vlc
```

With no argument, a graph is generated from all installed packages.

As an output a ```packages.dot``` file is created. You must call Graphviz to generate a png file.
```sh
dot -Tpng packages.dot > packages.png
```


<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Romain Giraud - dev@romaingiraud.com

Project Link: [https://gitlab.com/RomainGiraud/ppgg](https://gitlab.com/RomainGiraud/ppgg)
